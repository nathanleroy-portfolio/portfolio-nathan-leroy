# Nathan LEROY - Portfolio

Etudiant en **microélectronique** et **automatisme**

Ecole d'ingénieur **Polytech Montpellier** 

---

Bonjour et bienvenue sur mon Portfolio. 

Vous trouverez ici un tour d'horizon de mes projets scolaires, personnels et de mes autres centres d'intérêts. 


## Projets Polytech 

**Projet de fin d'étude : Automatisation d'une micro brasserie**

> _Septembre 2021 - Janvier 2022_
>
> Le PFE est un projet en cours couvrant 400 heures minimum sur le semestre. Il s'agit d'un projet d'automatisation nécessitant la mise en en place d'une interface web ainsi que le contrôle du processus de brassage via une série de capteurs et de commandes. Une présentation complète du projet est disponible [ici](https://gitlab.com/NathanLEROY/Portfolio/-/blob/main/Projets%20Polytech/CdC_PFE_Micro_Brasserie.pdf).  


* **Projet semestriel : Canne avertisseuse low energy**

> _Février 2021 - Mai 2021_
> 
> Le projet : Canne avertisseuse low energy était un projet en colaboration avec l'insitut Supagro Montpellier, dans le cadre de la production du foin de Crau, le seul foin à appelation d'origine contrôlé. 
> L'objectif était d'optimiser une canne qui détecte un passage d'eau, envoie une trame de données pour avertir l'utilisateur, puis se mette en mode sleep et ce pendant une période de 4 mois sans changer de batterie. Le prototype de Supagro utilisait une batterie de voiture 12 Volt, nous sommes passé sur deux batteries 18650 pour toute la saison. Sur ce projet, j'ai été chargé de transférer le code initialement sur Arduinon vers un STM32WL55JC, qui possèdait déjà un module LORA ainsi qu'un très bon mode sleep. Grâce à ce projet j'ai pu consolider mes notions en C ainsi que mes compétences de programmation embarqué. 

* **Projet robotique : Developpement sur BlueRov**
 
> _Février 2021 - Mai 2021_
>
> Dans le cadre du module de robotique, j'ai pu travailler avec 4 autres étudiants sur le drone sous-marin BlueRov. J'avais pour objectif avec un autre étudiant d'implémenter les commandes de celui-ci sur une GUI, dans le cas présent codé en QT, et d'afficher les différentes données du robot (pression, vitesse, température, batterie, ...), à la manière de QGC (QGroundControl). Ce travail m'aura été particulièrement utile lors de mon stage chez Merio, l'entreprise utilisant des outils similaires pour ses propres logiciels. 

* **Projet scolaire : Developpement d'un jeux sur Basys3**

> _Fevrier 2021 - Juin 2021_
>
> Dans le cadre de l'enseignement de Système Embarqué, j'ai eu pour projet de créer un jeux-vidéo sur une Basys3 en VHDL. L'objectif était de prendre en main les outils d'entrée/sortie sur un FPGA notamment sur la sortie VGA, pour la vidéo. J'ai choisit de réaliser un jeux similaire à _Dance Dance Revolution_, qui prenait en compte l'affichage des sprites, la gestion de 4 boutons et 5 switch, l'utilisation de led, d'une horloge et d'un afficheur digital. J'ai acquit grâce à ce projet une bonne maitrise du VHDL et de l'outil Vivado.

<!--
* **Projet semestriel : Player Audio**

* **Projet scolaire : Automatisation d'un paletiseur en OSTR**

* **Projet semestriel : Roulette pilotée**
-->

## Projets personnels 

### Associatif

* **Polytech Montpellier : FEDERP, BDA, K-Fet et Fresque du Climat**
> _Septembre 2017 - Juillet 2022_
>
> Durant mes 5 années d'études à l'école Polytech Montpellier, je me suis beaucoup investi dans la vie associative autant artistique qu'étudiante. J'ai pu ainsi dévelloper de nombreuses compétences humaines et techniques tel que :
>* Communication : réseaux sociaux, montage audio et video (Photoshop, Premiere Pro, Studio One, Canva) et prise de parole en public. 
>* Management : Outils de communication interne (Pack google, Trello, Mattermost), gestion d'équipe, animation et gestion de crise.
>* Gestion : Gestion de l'admnistratif, bilan et suivit financier 
>
> Vous pourrez retrouver [ici](https://gitlab.com/NathanLEROY/Portfolio/-/blob/main/Projets%20Personnels%20et%20Centre%20d'Int%C3%A9r%C3%AAts/L_associatif_%C3%A0_Polytech.pdf) les différents projets et associations dans lesquels j'ai pu m'investir à Polytech. 

<!--
* **Lycée : MDL, scène ouverte**
-->

### Artistique 

* **Musique**

> _Depuis 2012_
>
> Bassiste depuis 9 ans, j'ai pu m'investir dans plus d'une dizaine de groupe oscillant de la pop au jazz, du métal à la bossa. Outre cela, je me suis ouvert au piano, à la guitare ou à la batterie. Grâce à cela, j'ai pu jouer dans des salles comme le Corum ou le Rockstore de Montpellier, dans des salles allant jusqu'à 1500 personnes. En parrallèle, j'ai aidé à la mise en place de plusieurs petits concerts, écrit et composé pour des courts métrages d'animation, des jeux-vidéos ou des créations personnels. La musique m'a permit de m'ouvrir aux autres, d'apprendre à être très rigoureux, notamment en jouant avec des compositions d'une dizaine de personnes et mettre en place des feuilles de routes (matériel, passage, organisation). 

* **Théâtre**

> _Septembre 2010 - Aout 2017_
>
> J'ai été comédien amateur pendant plus de 7 ans, d'abord dans un club d'improvisation, puis dans une troupe jeune amatrice, les Zazous de Castries. J'ai acquit grâce à cela une très bonne aisance à l'oral, qui m'aura beaucoup servit pour mes oraux. En outre, j'ai aidé à l'écriture et la mise en scène de plusieurs pièces, mis en place 4 festivals amateurs, **Alerte Rouge** et effectué un échange Franco-Allemand de deux semaines, entièrement en anglais, autour du théâtre. Cela demandait un bon niveau en anglais et m'a permis de beaucoup progresser. 

<!--
### Centres d'intérêts 
-->
